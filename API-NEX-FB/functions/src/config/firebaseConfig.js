import * as admin from 'firebase-admin';
import * as fireorm from 'fireorm';
require('custom-env').env();

const serviceAccount = require(process.env.FIREBASE_CONFIG);

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: 'https://express-eb7ec.firebaseio.com',
});

const firestore = admin.firestore();
firestore.settings({
    timestampsInSnapshots: true,
});
fireorm.Initialize(firestore);

module.exports = firestore;