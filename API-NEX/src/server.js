const express = require('express')
const app = express()
 
app.use(require('./route/index'));
 
var server = app.listen(3000, () => {
  console.log('Listening on port ' + server.address().port);
});