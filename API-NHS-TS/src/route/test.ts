const Joi = require('joi');
import testcontroller from "../controller/testController"

export default [
    {
        method: 'POST',
        path: '/',
        options: {
            handler: testcontroller.test,
            description: 'Follow a course',
            notes: 'Follow a course',
            tags: ['api', 'Client', 'follow'],
            auth: false,
            validate: {
                // headers: Joi.object({
                //     Authorization: Joi.string().description('Authentication Token')
                // }).options({ allowUnknown: true }),
                payload: {
                    course: Joi.string().description('courseId'),
                },
                failAction: testcontroller.test
            },
        },
    }
]