const { Collection, getRepository } = require('fireorm');
const fireStore = require('../config/firebaseConfig');

module.exports = {
    test: async (req, res, err) => {
        try {
            @Collection()
            class Todo {
                constructor(id, name) {
                    this.id = id;
                    this.name = name;
                }
            }

            const todoRepository = getRepository(Todo, fireStore);

            const todo = new Todo();
            todo.text = "Check fireorm's Github Repository";
            todo.done = false;

            const todoDocument = await todoRepository.create(todo); // Create
            // const mySuperTodoDocument = await todoRepository.findById(todoDocument.id); // Read
            // await todoRepository.update(mySuperTodoDocument); // Update
            // await todoRepository.delete(mySuperTodoDocument.id); // Delete
        } catch (e) {
            console.log(e);
            res.send(e);
        }
    }
};