let router = require('express').Router();
let { test } = require('../controller/testController');

router.get('/get', test);

module.exports = router;