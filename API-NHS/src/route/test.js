const Joi = require('joi');
var { test } = require('../controller/testController.js');

module.exports = [
    {
        method: 'POST',
        path: '/',
        options: {
            handler: test,
            description: 'Follow a course',
            notes: 'Follow a course',
            tags: ['api', 'Client', 'follow'],
            auth: false,
            validate: {
                // headers: Joi.object({
                //     Authorization: Joi.string().description('Authentication Token')
                // }).options({ allowUnknown: true }),
                payload: {
                    course: Joi.string().description('courseId'),
                },
                failAction: test
            },
        },
    }
];