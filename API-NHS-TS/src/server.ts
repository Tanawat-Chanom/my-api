'use strict';
var routes = require('./route/index.js');
const Hapi = require('@hapi/hapi');
const Inert = require('@hapi/inert');
const Vision = require('@hapi/vision');
const HapiSwagger = require('hapi-swagger');
const Pack = require('../package');

/**
 * Configuration
 */
const port = 3000;
const host = 'localhost';

const init = async () => {
    const server = Hapi.server({
        port: port,
        host: host,
        routes: {
            cors: {
                origin: ["*"],
                additionalHeaders: ["locale"]
            }
        }
    });

    const swaggerOptions = {
        info: {
            title: 'Test API Documentation',
            version: Pack.version,
        },
    };

    await server.register([
        Inert,
        Vision,
        {
            plugin: HapiSwagger,
            options: swaggerOptions
        }
    ]);

    server.route(routes);

    await server.start();
    console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});

init();
